import Vue from "vue";

import VueRouter from "vue-router";

import store from "../store/index";
import LoginView from "../views/LoginView.vue";
import PhotosView from "../views/PhotosView.vue";
import PostsView from "../views/PostsView.vue";
import UserEditView from "../views/UserEditView.vue";
import UsersView from "../views/UsersView.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/photos",
    name: "PhotosView",
    component: PhotosView,
    meta: { requiresAuth: true }
  },
  {
    path: "/posts",
    name: "PostsView",
    component: PostsView,
    meta: { requiresAuth: true }
  },
  {
    path: "/users",
    name: "UsersView",
    component: UsersView,
    meta: { requiresAuth: true },
  },
  {
    path: "/users/edit/:id",
    name: "UserEditView",
    component: UserEditView,
    meta: { requiresAuth: true },
  },
  {
    path: "/",
    name: "LoginView",
    component: LoginView,
  },
];


const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.user) {
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})
export default router;
