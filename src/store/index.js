import Vue from "vue";

import Vuex from "vuex";

import createPersistedState from "vuex-persistedstate";

import { getResources } from "../assets/API";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    posts: [],
    photos: [],
    users: [],
    user: null,
  },
  mutations: {
    SET_PHOTOS(state, photos) {
      state.photos = photos;
    },
    SET_USERS(state, getUsers) {
      !state.users.some(item => item.page === getUsers.page) &&
        state.users.push(getUsers);
    },
    SET_POSTS(state, posts) {
      state.posts = posts;
    },
    SET_AUTORISEUSER(state, user) {
      state.user = user;
    },
    ADD_USERS(state, user) {
      !state.users[0].data.some(item => item.email === user.email) &&
        state.users[0].data.push(user);
    },

    DELETE_USERS(state, id) {
      const index = state.users[0].data.indexOf(
        state.users[0].data.find(item => item.id === id)
      );
      state.users[0].data.splice(index, 1);
    },
    EDIT_USERS(state, data) {
      let user = state.users[0].data.find(item => item.id === +data.id);
      user.first_name = data.first_name;
      user.last_name = data.last_name;
      user.email = data.email;
    }
  },
  actions: {
    getPhotos({commit}) {
      getResources("https://api.unsplash.com/photos/?client_id=JspLe1rixYrz3ScD0JoiOCJbCQsfToePo-vbZsdsteA").then(data => {
        commit("SET_PHOTOS", data);
      });
    },
    getUsers({ commit },page) {
      getResources(`https://reqres.in/api/users?page=${page}`).then(
        data => {
          commit("SET_USERS", data);
        }
      );
    },
    getPosts({ commit }) {
      getResources("https://jsonplaceholder.typicode.com/posts").then(data => {
        commit("SET_POSTS", data);
      });
    },
    autoriseUser({ commit }, data) {
      commit("SET_AUTORISEUSER", data);
    },
    addUsers({ commit }, user) {
      commit("ADD_USERS", user);
    },

    deleteUsers({ commit }, id) {
      commit("DELETE_USERS", id);
    },
    editUsers({ commit }, data) {
      // console.log(data)
      commit("EDIT_USERS", data);
    }
  },

  getters: {
    getData(state) {
      let allUsers = state.users.flatMap(item => {
        return item.data;
      });
      return allUsers;
    }
  },

  plugins: [createPersistedState()]
});
