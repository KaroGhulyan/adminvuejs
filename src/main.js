import Vue from "vue";

import { VueMasonryPlugin } from "vue-masonry";

import App from "./App.vue";
import router from "./router";
import store from "./store";

import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;
Vue.use(VueMasonryPlugin);

new Vue({
  router,
  store,
  VueMasonryPlugin,
  vuetify,
  render: h => h(App)
}).$mount("#app");
